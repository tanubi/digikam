/* ============================================================
 *
 * This file is a part of digiKam project
 * https://www.digikam.org
 *
 * Date        : 2020-07-26
 * Description : System settings container.
 *
 * SPDX-FileCopyrightText: 2020 by Maik Qualmann <metzpinguin at gmail dot com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * ============================================================ */

#include "systemsettings.h"

// Qt includes

#include <QSettings>
#include <QStandardPaths>

// Local includes

#include "digikam_debug.h"
#include "digikam_config.h"

namespace Digikam
{

SystemSettings::SystemSettings(const QString& name)
    : useHighDpiScaling(false),
      useHighDpiPixmaps(false),
      enableFaceEngine (false),
      enableAesthetic  (false),
      enableAutoTags   (false),
      enableLogging    (false),
      disableOpenCL    (true)
{
    if (!name.isEmpty())
    {
        m_path = QStandardPaths::writableLocation(QStandardPaths::GenericConfigLocation) +
                 QLatin1Char('/') + name + QLatin1String("_systemrc");
    }

    readSettings();
}

SystemSettings::~SystemSettings()
{
}

void SystemSettings::readSettings()
{
    if (m_path.isEmpty())
    {
        return;
    }

    QSettings settings(m_path, QSettings::IniFormat);

    settings.beginGroup(QLatin1String("System"));

#ifdef Q_OS_LINUX

    useHighDpiScaling    = settings.value(QLatin1String("useHighDpiScaling"), true).toBool();
    useHighDpiPixmaps    = settings.value(QLatin1String("useHighDpiPixmaps"), true).toBool();

#else

    useHighDpiScaling    = settings.value(QLatin1String("useHighDpiScaling"), false).toBool();
    useHighDpiPixmaps    = settings.value(QLatin1String("useHighDpiPixmaps"), false).toBool();

#endif

    if (settings.contains(QLatin1String("disableFaceEngine")))
    {
        bool oldSetting  = settings.value(QLatin1String("disableFaceEngine"), false).toBool();
        enableFaceEngine = !oldSetting;
    }
    else
    {
        enableFaceEngine = settings.value(QLatin1String("enableFaceEngine"),  true).toBool();
    }

    enableAesthetic      = settings.value(QLatin1String("enableAesthetic"),   true).toBool();
    enableAutoTags       = settings.value(QLatin1String("enableAutoTags"),    true).toBool();
    enableLogging        = settings.value(QLatin1String("enableLogging"),     false).toBool();
    disableOpenCL        = settings.value(QLatin1String("disableOpenCL"),     true).toBool();
    settings.endGroup();
}

void SystemSettings::saveSettings()
{
    if (m_path.isEmpty())
    {
        return;
    }

    QSettings settings(m_path, QSettings::IniFormat);

    settings.beginGroup(QLatin1String("System"));
    settings.setValue(QLatin1String("useHighDpiScaling"), useHighDpiScaling);
    settings.setValue(QLatin1String("useHighDpiPixmaps"), useHighDpiPixmaps);
    settings.setValue(QLatin1String("enableFaceEngine"),  enableFaceEngine);
    settings.setValue(QLatin1String("enableAesthetic"),   enableAesthetic);
    settings.setValue(QLatin1String("enableAutoTags"),    enableAutoTags);
    settings.setValue(QLatin1String("enableLogging"),     enableLogging);
    settings.setValue(QLatin1String("disableOpenCL"),     disableOpenCL);

    if (settings.contains(QLatin1String("disableFaceEngine")))
    {
        settings.remove(QLatin1String("disableFaceEngine"));
    }

    settings.endGroup();
}

} // namespace Digikam
